let number = Number(prompt("Please provide a number: "));

console.log(`The number you provided is ${number}.`);

for(let val = number; val > 0; val--){

    if (val % 10 === 0) {
        console.log(`The number is divisible by 10. Skipping the number.`);
        continue;
    }

    if(val % 5 === 0){
        console.log(val);
    }
    
    if (val <= 50) {
        console.log(`The current value is at 50. Terminating the loop.`);
        break;
    }
}

const myString = "supercalifragilisticexpialidocious";
let consonants = "";

console.log(myString);

const vowelsRegEx = /[aeiou]/i;

for (let i = 0; i < myString.length; i++) {

    if(vowelsRegEx.test(myString[i])) {
        continue;
    } else {
        consonants = consonants + myString[i];
    }
}

console.log(consonants);

